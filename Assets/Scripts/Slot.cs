﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{

    private Inventory inventory;
    public int index;
    //public int maxHealth = 100;
    //public int currentHealth;
    public int healthGain = 20;
    public JasonPlayerController jasonPlayer;
    public HealthBar healthBar;

    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Jason").GetComponent<Inventory>();
        jasonPlayer = GameObject.FindGameObjectWithTag("Jason").GetComponent<JasonPlayerController>();
    }

    private void Update()
    {
        if (transform.childCount <= 0)
        {
            inventory.items[index] = 0;
        }
    }

    public void DropItem()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void UseItem()
    {
        foreach (Transform child in transform)
        {
            gainHealth(20);
            GameObject.Destroy(child.gameObject);
        }
    }

    public void gainHealth(int hp)
    {
        jasonPlayer.currentHealth += hp;
        healthBar.SetHealth(jasonPlayer.currentHealth);
    }
}
