﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Quest : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject QuestUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            
            if (GameIsPaused)
            {
                QuestUI.SetActive(false);
                Time.timeScale = 1f;
                GameIsPaused = false;
               // QuestUI.SetActive(!QuestUI.activeSelf);
            }
            else
            {
                QuestUI.SetActive(true);
                Time.timeScale = 0f;
                GameIsPaused = true;
            }
        }
    }


}