﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int health;
    public int xp;
    public int xbar;
    public int hbar;
    public float[] position;

    public PlayerData (JasonPlayerController jasonPlayer)
    {
        health = jasonPlayer.currentHealth;
        xp = jasonPlayer.currentXP;
        position = new float[3];
        position[0] =  jasonPlayer.transform.position.x;
        position[1] =  jasonPlayer.transform.position.y;
        position[2] = jasonPlayer.transform.position.z;
    }
}
