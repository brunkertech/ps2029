﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSystem : MonoBehaviour
{
    private JasonPlayerController jason;

    // Start is called before the first frame update
    void Start()
    {
       jason = GameObject.FindGameObjectWithTag("Jason").GetComponent<JasonPlayerController>();
       jason.LoadPlayer();
    }
}
