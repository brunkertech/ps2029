﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stats : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject StatUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {

            if (GameIsPaused)
            {
                StatUI.SetActive(false);
                Time.timeScale = 1f;
                GameIsPaused = false;
                // QuestUI.SetActive(!QuestUI.activeSelf);
            }
            else
            {
                StatUI.SetActive(true);
                Time.timeScale = 0f;
                GameIsPaused = true;
            }
        }
    }

    public void statFreeze()
    {
        StatUI.SetActive(true);
        Time.timeScale = 0f;
    }


}
