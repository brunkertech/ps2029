﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swordman : PlayerController
{
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public int attackDamage = 20;
    public LayerMask enemyLayers;
    public int minXP = 0;
    public int maxXP = 100;
    public int currentXP;
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar;
    public ExperienceBar experienceBar;
    bool isDead;
    public GameObject levelIcon;

    private void Start()
    {
        _CapsulleCollider = this.transform.GetComponent<CapsuleCollider2D>();
        _Anim = this.transform.Find("model").GetComponent<Animator>();
        _rigidbody = this.transform.GetComponent<Rigidbody2D>();

        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        currentXP = minXP;
        experienceBar.SetMinXP(minXP);
    }


    private void Update()
    {
        checkInput();

        if (_rigidbody.velocity.magnitude > 30)
        {
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x - 0.1f, _rigidbody.velocity.y - 0.1f);

        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    public void checkInput()
    {

        if (!PauseMenu.GameIsPaused)
        {

            if (Input.GetKeyDown(KeyCode.S))
            {

                IsSit = true;
                _Anim.Play("Sit");


            }
            else if (Input.GetKeyUp(KeyCode.S))
            {

                _Anim.Play("Idle");
                IsSit = false;
            }


            if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Sit") || _Anim.GetCurrentAnimatorStateInfo(0).IsName("Die"))
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (currentJumpCount < JumpCount)  // 0 , 1
                    {
                        DownJump();
                    }
                }

                return;
            }


            _MoveX = Input.GetAxis("Horizontal");

            GroundCheckUpdate();


            if (!_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                    if (Input.GetKey(KeyCode.Mouse0))
                    {
                        _Anim.Play("Attack");
                        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
                        Collider2D[] hitEagle = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

                        foreach (Collider2D enemy in hitEnemies)
                        {
                            enemy.GetComponent<Enemy>().TakeDamage(attackDamage);                           
                        }                     
                    }
                    else
                    {
                        if (_MoveX == 0)
                        {
                            if (!OnceJumpRayCheck)
                                _Anim.Play("Idle");

                        }
                        else
                            _Anim.Play("Run");
                    }
            }

                if (Input.GetKey(KeyCode.D))
                {

                    if (isGrounded)
                    {

                        if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                            return;

                        transform.transform.Translate(Vector2.right * _MoveX * MoveSpeed * Time.deltaTime);


                    }
                    else
                    {

                        transform.transform.Translate(new Vector3(_MoveX * MoveSpeed * Time.deltaTime, 0, 0));

                    }


                    if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                        return;

                    if (!Input.GetKey(KeyCode.A))
                        Filp(false);


                }
                else if (Input.GetKey(KeyCode.A))
                {


                    if (isGrounded)
                    {
                        if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                            return;

                        transform.transform.Translate(Vector2.right * _MoveX * MoveSpeed * Time.deltaTime);

                    }
                    else

                        transform.transform.Translate(new Vector3(_MoveX * MoveSpeed * Time.deltaTime, 0, 0));


                    if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                        return;

                    if (!Input.GetKey(KeyCode.D))
                        Filp(true);
                }
            

            if (Input.GetKeyDown(KeyCode.W))
            {
                if (_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    return;
                if (currentJumpCount < JumpCount)  // 0 , 1
                {
                    if (!IsSit)
                        prefromJump();
                    else
                        DownJump();
                }

            }
        }
    }

    protected override void LandingEvent()
    {
        if (!_Anim.GetCurrentAnimatorStateInfo(0).IsName("Run") && !_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            _Anim.Play("Idle");
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    public void GainXP(int xp)
    {
        currentXP += xp;

        experienceBar.SetXP(currentXP);

        if (currentXP >= 100)
        {
            currentXP = 0;
            experienceBar.SetXP(currentXP);
            levelIcon.SetActive(true);
        }
    }

    void Death()
    {
        isDead = true;

        _Anim.Play("Die");
        //Destroy(transform.GetChild(2).gameObject,1);
    }
}
