﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSensor : MonoBehaviour
{

    public PlayerController _root;

    // Use this for initialization
    void Start()
    {
        _root = this.transform.root.GetComponent<PlayerController>();

    }



    ContactPoint2D[] contacts = new ContactPoint2D[1];

    void OnTriggerStay2D(Collider2D other)
    {


        if (other.CompareTag("Ground") || other.CompareTag("Block"))
        {

            if (other.CompareTag("Ground"))
            {
                _root.Is_DownJump_GroundCheck = true;

            }
            else
            {
                _root.Is_DownJump_GroundCheck = false;
            }

            if (_root._rigidbody.velocity.y <= 0)
            {

                _root.isGrounded = true;
                _root.currentJumpCount = 0;
            }


        }
    }

    void OnTriggerExit2D(Collider2D other)
    {

        _root.isGrounded = false;

    }



}
