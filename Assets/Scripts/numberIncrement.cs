﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class numberIncrement : MonoBehaviour
{
    public int numinc;
    public Text incText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        incText.text = numinc.ToString();
    }

    public void Increment()
    {
        numinc++;
    }

    public void Decrement()
    {
        numinc--;
    }
}
