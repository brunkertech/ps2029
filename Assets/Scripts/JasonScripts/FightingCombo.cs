﻿using UnityEngine;
using UnityEngine.Events;

public class FightingCombo : MonoBehaviour
{
    public Animator anim;
    public int numOfClicks = 0;
    private float lastClickedtime = 0;
    private float maxComboDelay = 1.2f;
    [SerializeField] private LayerMask enemyLayers;     //enemy layer
    public float attackRange = 0.5f;
    public int attackDamage = 20;
    public Transform attackPoint;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time - lastClickedtime > maxComboDelay)
        {
            numOfClicks = 0;
        }

        if(Input.GetKeyDown("x"))
        {
            lastClickedtime = Time.time;
            numOfClicks++;
            if(numOfClicks == 1)
            {
                anim.SetBool("SwordAttack1", true);                
            }
            numOfClicks = Mathf.Clamp(numOfClicks, 0, 2);
        }
    }

    public void return1()
    {
        if (numOfClicks >= 2)
        {
            anim.SetBool("SwordAttack2", true);
            Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

            foreach (Collider2D enemy in hitEnemies)
            {
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            }
        }
        else
        {
            anim.SetBool("SwordAttack1", false);
            numOfClicks = 0;
        }
    }

    public void return2()
    {
        anim.SetBool("SwordAttack1", false);
        anim.SetBool("SwordAttack2", false);
        numOfClicks = 0;
    }
}
