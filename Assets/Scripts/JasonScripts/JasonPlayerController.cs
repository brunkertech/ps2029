﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class JasonPlayerController : MonoBehaviour
{
    private Rigidbody2D _rb;                //Rigidbody component
    private Animator _anim;                 //Animator component
    private Collider2D _coll;               //Collider2D component
    private SpriteRenderer _sprender;       //SpriteRenderer component
    public Flowchart flowchart;
    
    private enum State {idle, running, jumping, falling };  //store the finite game states
    private State state = State.idle;                       //default state set to idle

    [SerializeField] private LayerMask enemyLayers;     //enemy layer
    [SerializeField] private LayerMask ground;          //ground layer

    [SerializeField] private float speed = 3f;          //speed when player moves
    [SerializeField] private float jumpForce = 10f;     // force with which player jumps
    [SerializeField] private int arrows = 0;            //number of arrows collected
    [SerializeField] private Text arrowsNumber;         //arrow text   
    public bool isReady = false;                        //set true when the dialogue prompt is over
    public bool canAttack = false;

    public float attackRange = 0.5f;
    public int attackDamage = 20;
    private int minXP = 0;                  //minimum player experience
    //private int maxXP = 100;                //maximum player experience
    public int currentXP;                  //player current experience
    private int maxHealth = 100;             //maximum player health
    public int currentHealth;               //player current health
    [SerializeField] private HealthBar healthBar;             //healthbar object
    [SerializeField] private ExperienceBar experienceBar;    //XPbar object
    public GameObject levelIcon;            //current XP level
    private bool isDead;                    //checks player's death status
    public Transform attackPoint;

    //Start() runs when the game starts
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();   //assign the Rigidbody component to rb
        _anim = GetComponent<Animator>();    //assign the animator component to anim
        _coll = GetComponent<Collider2D>();  //assign the Collider2D component to coll
  
        //initialize health and healthbar
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);

        //initialize XPbar
        currentXP = minXP;
        experienceBar.SetMinXP(minXP);
    }

    //Update() keeps updating
    private void Update()
    {
        if (isReady)
        {
            movement();
            AnimationState();
        }
        _anim.SetInteger("state", (int)state);

        //HP test
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            TakeDamage(20);
        }

        //XP test
        if (Input.GetKeyDown(KeyCode.W))
        {
            GainXP(20);
        }
    }

    //If collided with an arrow or sword, collect and update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Collectible")
        {
            Destroy(collision.gameObject);
            arrows += 1;
            arrowsNumber.text = arrows.ToString();
        }

        if (collision.tag == "Sword")
        {
            Destroy(collision.gameObject);
            isReady = false;
            state = State.idle;
            flowchart.ExecuteBlock("Attack");
            canAttack = true;
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    //player movement based on user input
    private void movement()
    {
        float hDirection = Input.GetAxis("Horizontal");

        //if hDirection <0 (left arrow pressed), go to left
        if (hDirection < 0)
        {
            _rb.velocity = new Vector2(-speed, _rb.velocity.y);
            transform.localScale = new Vector2(-0.4f, 0.4f);
        }
        else if (hDirection > 0)
        {
            _rb.velocity = new Vector2(speed, _rb.velocity.y);
            transform.localScale = new Vector2(0.4f, 0.4f);
        }
        else
        {
            _rb.velocity = new Vector2(0, _rb.velocity.y);
        }

        if (Input.GetButtonDown("Jump") && _coll.IsTouchingLayers(ground))
        {
            _rb.velocity = new Vector2(_rb.velocity.x, jumpForce);
            state = State.jumping;
        }

        if (canAttack)
        {
            //attack when x is pressed
            if (Input.GetKeyDown("x"))
            {
                _anim.SetTrigger("attack");

            }
        }

        //attack when f is pressed
        if (Input.GetKeyDown("f") && arrows > 0)
        {
            _anim.SetTrigger("shoot");
            arrows--;
            arrowsNumber.text = arrows.ToString();
        }
    }

    void jasonHitSync()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (Collider2D enemy in hitEnemies)
        {
            if (!enemy.GetComponent<Enemy>().IsDead)
            {
                enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
            }
        }
        //enemy.GetComponent<Enemy>().TakeDamage(attackDamage);
    }

    //decides which animation to perform
    private void AnimationState()
    {
        if (state == State.jumping)
        {
            if (_rb.velocity.y < .1f)
            {
                state = State.falling;
            }
        }
        else if (state == State.falling)
        {
            if (_coll.IsTouchingLayers(ground))
            {
                state = State.idle;
            }
        }
        else if (Mathf.Abs(_rb.velocity.x) > 2f)
        {
            //Moving
            state = State.running;
        }
        else
        {
            state = State.idle;
        }
    }

    //When dialogue prompt is over, get ready for animation
    public void SetReady()
    {
        isReady = true;
    }


    //runs when player take a hit
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    //death animation
    private void Death()
    {
        isDead = true;
        _sprender = gameObject.GetComponent<SpriteRenderer>();
        _sprender.enabled = false;
        isReady = false;
    }

    //xp update
    private void GainXP(int xp)
    {
        currentXP += xp;

        experienceBar.SetXP(currentXP);

        if (currentXP >= 100)
        {
            currentXP = 0;
            experienceBar.SetXP(currentXP);
            levelIcon.SetActive(true);
        }
    }

    ///save current stats
    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
    }

    //load from saved instance
    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        currentHealth = data.health;
        healthBar.SetHealth(currentHealth);
        currentXP = data.xp;
        experienceBar.SetXP(currentXP);

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;
    }

    public void LoadJason()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        currentHealth = data.health;
        healthBar.SetHealth(currentHealth);
        currentXP = data.xp;
        experienceBar.SetXP(currentXP);
        canAttack = true;
    }
}
