﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public JasonPlayerController playerHealth;
    public static bool GameIsOver = true;
    public GameObject gameOverUI;

	void Update()
	{
		if (playerHealth.currentHealth <= 0)
		{
			if (GameIsOver)
			{
				GameEnd();
			}
		}
	}


	public void GameEnd() //Displays screen when player is dead
	{
		gameOverUI.SetActive(true);
		GameIsOver = true;
		Time.timeScale = 1f;
	}

	public void LoadMenu()
	{
		SceneManager.LoadScene("Menu");
	}

	public void Retry()//For Knight in Town map
	{
		SceneManager.LoadScene("Beach");
	}
}
