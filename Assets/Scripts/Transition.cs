﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Fungus;

public class Transition : MonoBehaviour
{
    public GameObject loadingScreen;
    public GameObject playerObject;
    private JasonPlayerController jason;
    public Slider slider;
    public Text progressText;
    [SerializeField] private string newLevel;
    [SerializeField] private Flowchart flowchart;

    void Start()
    {
        jason = GameObject.FindGameObjectWithTag("Jason").GetComponent<JasonPlayerController>();
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Jason"))
        {
            if (jason.canAttack)
            {
                LoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
                jason.SavePlayer();
            }
            else
            {
                flowchart.ExecuteBlock("WallBlock");
            }
        }
    }


    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }


    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;
            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }
}
