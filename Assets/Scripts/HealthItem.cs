﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : MonoBehaviour
{
    private JasonPlayerController jason;
    private HealthBar healthBar;

    // Start is called before the first frame update
   private void Start()
    {
        jason = GameObject.FindGameObjectWithTag("Jason").GetComponent<JasonPlayerController>();
    }

    public void gainHealth(int hp)
    {
        jason.currentHealth += hp;

        healthBar.SetHealth(jason.currentHealth);
    }

    public void Use()
    {
      //  gainHealth(20);
        Destroy(gameObject);
    }
}
