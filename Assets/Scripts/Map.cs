﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Map : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject MapUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {

            if (GameIsPaused)
            {
                MapUI.SetActive(false);
                Time.timeScale = 1f;
                GameIsPaused = false;
                // QuestUI.SetActive(!QuestUI.activeSelf);
            }
            else
            {
                MapUI.SetActive(true);
                Time.timeScale = 0f;
                GameIsPaused = true;
            }
        }
    }


}
