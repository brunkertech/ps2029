﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    public float speed;
    public GameObject player;
    private SpriteRenderer spriteRenderer;
    public Transform playerCharacter;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public float attackRate = 2f;
    float nextAttackTime = 0f;
    public LayerMask playerLayer;
    private Swordman swordman;
    public ExperienceBar experienceBar;

    public Animator animator;

    public int maxHealth = 100;
    int currentHealth;
    public int attackDamage = 10;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
        swordman = GameObject.FindGameObjectWithTag("Jason").GetComponent<Swordman>();
    }

    void Update()
    {
        EnemyFollow();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

       // animator.SetTrigger("Hurt");

        if (currentHealth <= 0)
        {
            Die();
            //swordman.TakeDamage(100);
        }
    }

    void Die()
    {
        UnityEngine.Debug.Log("Enemy died!");

       // animator.SetBool("IsDead", true);

        gainXP(50);

        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
    }

    void EnemyFollow()
    {
        this.spriteRenderer.flipX = playerCharacter.transform.position.x > this.transform.position.x;
        if (Vector2.Distance(transform.position, player.transform.position) < 4)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            animator.Play("Eagle_Animation");
            if (Time.time >= nextAttackTime)
            {
                if (Vector2.Distance(transform.position, player.transform.position) < 1)
                {
                    speed = 0f;
                    //animator.Play("Eagle_Idle");

                    Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);

                    foreach (Collider2D player in hitPlayer)
                    {

                        if (player.GetComponent<Swordman>().currentHealth <= 0)
                        {
                            animator.Play("Eagle_Animation");
                        }
                    }
                    nextAttackTime = Time.time + 5f / attackRate;
                }
                else
                {
                    speed = 1f;
                }
            }
        }
        else
        {
            animator.Play("Eagle_Animation");
        }
    }

    void hitSync()
    {
        player.GetComponent<Swordman>().TakeDamage(attackDamage);
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    public void gainXP(int xp)
    {
        swordman.currentXP += xp;

        experienceBar.SetXP(swordman.currentXP);
    }
}
