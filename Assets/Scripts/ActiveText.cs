﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveText : MonoBehaviour
{
    public float sec;
    void Start()
    {
        if (gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        StartCoroutine(LateCall());
    }

    IEnumerator LateCall()
    {

        yield return new WaitForSeconds(sec);

        gameObject.SetActive(false);
        //Do Function here...
    }
}
