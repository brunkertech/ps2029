﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public GameObject player;
    private SpriteRenderer spriteRenderer;
    public Transform playerCharacter;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public float attackRate = 2f;
    float nextAttackTime = 0f;
    public LayerMask playerLayer;
    private JasonPlayerController jason;
    public ExperienceBar experienceBar;
    public bool IsDead = false;

    private Animator animator;

    public int maxHealth = 100;
    int currentHealth;
    public int attackDamage = 10;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        jason = GameObject.FindGameObjectWithTag("Jason").GetComponent<JasonPlayerController>();
    }

    void Update()
    {
        if (!IsDead)
        {
            EnemyFollow();
        }

    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        animator.SetTrigger("Hurt");

        if(currentHealth <= 0)
        {
            IsDead = true;
            Die();
        }
    }

    void Die()
    {
        animator.SetBool("IsDead", true);
        GameObject.Destroy(this.gameObject, 4);
        gainXP(50);

        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;

    }

    void EnemyFollow()
    {
        
        this.spriteRenderer.flipX = playerCharacter.transform.position.x > this.transform.position.x;
        if (Vector2.Distance(transform.position, player.transform.position) < 4)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            animator.SetInteger("AnimState", 2);
            if(Time.time >= nextAttackTime)
            { 
                if (Vector2.Distance(transform.position, player.transform.position) < 1.3)
                {
                    speed = 0f;
                    animator.Play("Attack");

                    Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);

                    foreach(Collider2D player in hitPlayer)
                    {
                        if (player.GetComponent<JasonPlayerController>().currentHealth <= 0)
                        {
                            animator.Play("Idle");
                        }
                    }
                    nextAttackTime = Time.time + 5f / attackRate;
                }
                else
                {
                    speed = 1f;
                }
            }
        }
        else
        {
            animator.SetInteger("AnimState", 0);
        }
    }

    void hitSync()
    {
        player.GetComponent<JasonPlayerController>().TakeDamage(attackDamage);
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    public void gainXP(int xp)
    {
        jason.currentXP += xp;

        experienceBar.SetXP(jason.currentXP);
        if (jason.currentXP >= 100)
        {
            jason.currentXP = 0;
            experienceBar.SetXP(jason.currentXP);
        }
    }
}
